import UIKit

protocol Humano {
    var nombre : String {get set}
    
    func correr()
    func comer()
    func dormir()
}

class Soldado : Humano{
    var nombre: String
    
    init(Soldadonombre Soldadonombre : String) {
        self.nombre = Soldadonombre
    }
    
    func correr() {
        print("Soldado \(nombre) esta corriendo")
    }
    
    func comer() {
        print("Soldado  \(nombre) esta comiendo")
    }
    
    func dormir() {
        print("Soldado  \(nombre) esta durmiendo")
    }
}

class Civil : Humano {
    var nombre: String
    
    init(Civilnombre Civilnombre : String) {
        self.nombre = Civilnombre
    }
    
    func correr() {
        print("\(nombre) esta corriendo")
    }
    
    func comer() {
        print("\(nombre) esta comiendo")
    }
    
    func dormir() {
        print("\(nombre) esta durmiendo")
    }
}

enum TipoHumano{
    case Soldado
    case Civil
}

class FabricaHumano{
    private static var sharedFabricaHumano = FabricaHumano()
    
    class func shared() -> FabricaHumano {
        return sharedFabricaHumano
    }
    
    func getHumano(HumanoType HumanoType : TipoHumano, Humanonombre Humanonombre : String)->Humano{
        switch HumanoType {
        case .Soldado:
            return Soldado(Soldadonombre: Humanonombre)
        case .Civil:
            return Civil(Civilnombre: Humanonombre)
        }
    }
}

let soldado = FabricaHumano.shared().getHumano(HumanoType: .Soldado, Humanonombre: "Pedro")
soldado.dormir()

let civil = FabricaHumano.shared().getHumano(HumanoType: .Civil, Humanonombre: "Alex")
civil.correr()